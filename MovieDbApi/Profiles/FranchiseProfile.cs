﻿using AutoMapper;
using MovieDbApi.Models;
using MovieDbApi.DTO.FranchiseDTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace MovieDbApi.Profiles
{
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {
            CreateMap<Franchise, FranchiseReadDTO>()

            .ForMember(cdto => cdto.Movies, opt => opt
            .MapFrom(c => c.Movies.Select(c => c.Id).ToArray()));


            CreateMap<FranchiseEditDTO, Franchise>();
           
            CreateMap<FranchiseCreateDTO, Franchise>();

        }
    }
}
