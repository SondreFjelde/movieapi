﻿using MovieDbApi.Models;
using MovieDbApi.DTO.CharacterDTO;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieDbApi.Profiles
{
    public class CharacterProfile : Profile
    {
        public CharacterProfile()
        {
            CreateMap<Character, CharacterReadDTO>()
                .ForMember(cdto => cdto.Movies, opt => opt
                .MapFrom(c => c.Movies.Select(c => c.Id).ToArray()));

            CreateMap<CharacterCreateDTO, Character>();
            CreateMap<CharacterEditDTO, Character>();

        }
    }
}
