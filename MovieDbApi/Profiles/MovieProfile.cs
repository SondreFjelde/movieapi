﻿using AutoMapper;
using MovieDbApi.Models;
using MovieDbApi.DTO.MovieDTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace MovieDbApi.Profiles
{
    public class MovieProfile : Profile
    {
        public MovieProfile()
        {
            CreateMap<Movie, MovieReadDTO>()
                .ForMember(mdto => mdto.Characters, opt => opt
              .MapFrom(m => m.Characters.Select(c => c.Id).ToArray()));

            //CreateMap<>


            CreateMap<MovieCreateDTO, Movie>();

            CreateMap<MovieEditDTO, Movie>();
                   
        }
    }
}
