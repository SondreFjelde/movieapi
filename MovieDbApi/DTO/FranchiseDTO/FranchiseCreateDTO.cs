﻿using System.ComponentModel.DataAnnotations;

namespace MovieDbApi.DTO.FranchiseDTO
{
    public class FranchiseCreateDTO
    {

        public string? Name { get; set; }
        public string? Description { get; set; }
    }
}
