﻿using System.ComponentModel.DataAnnotations;

namespace MovieDbApi.DTO.CharacterDTO
{
    public class CharacterCreateDTO
    {
         public string? Name { get; set; }
        public string? Alias { get; set; }
         public string? Gender { get; set; }
        public string? PictureLink { get; set; }
    }
}
