﻿using System.ComponentModel.DataAnnotations;

namespace MovieDbApi.DTO.CharacterDTO
{
    public class CharacterEditDTO
    {


        public int Id { get; set; }
         public string? Name { get; set; }
         public string? Alias { get; set; }
         public string? Gender { get; set; }
         public string? PictureLink { get; set; }
    }
}
