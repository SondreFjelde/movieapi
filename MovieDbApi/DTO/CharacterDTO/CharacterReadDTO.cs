﻿using System.ComponentModel.DataAnnotations;

namespace MovieDbApi.DTO.CharacterDTO
{
    public class CharacterReadDTO
    {


        public int Id { get; set; }
         public string? Name { get; set; }
         public string? Alias { get; set; }
         public string? Gender { get; set; }
         public string? PictureLink { get; set; }
         public List<string>? Movies { get; set; }
    }
}
