﻿using MovieDbApi.Models;
using System.ComponentModel.DataAnnotations;

namespace MovieDbApi.DTO.MovieDTO
{
    public class MovieCreateDTO
    {

      
        public string? Title { get; set; }
        public string? Genre { get; set; }
        public int? ReleaseYear { get; set; }
        public string? Director { get; set; }
        public string? Picture { get; set; }
        public string? TrailerLink { get; set; }
        public int? FranchiseId { get; set; }

    }
}
