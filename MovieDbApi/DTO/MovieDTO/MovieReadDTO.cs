﻿namespace MovieDbApi.DTO.MovieDTO
{
    public class MovieReadDTO
    {

        public int Id { get; set; }
        public string? Title { get; set; }
        public string? Genre { get; set; }
        public int? ReleaseYear { get; set; }
        public string? Director { get; set; }
        public string? Picture { get; set; }
        public string? TrailerLink { get; set; }
        public string? FranchiseId { get; set; }
        public List<string>? Characters { get; set; }

    }
}
