using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using MovieDbApi;
using MovieDbApi.Models;
using MovieDbApi.Services;
using System.Reflection;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddAutoMapper(typeof(Program).Assembly);
builder.Services.AddScoped(typeof(IMovieService), typeof(MovieService));
builder.Services.AddScoped(typeof(ICharacterService), typeof(CharacterService));
builder.Services.AddScoped(typeof(IFranchiseService), typeof(FranchiseService));
builder.Services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("v1", new OpenApiInfo
    {
        Title = "MovieDbApi",
        Version = "v1",
        Description = "Assignment for Fullstack Bootcamp with ASP.NET Core & Entity Framework.",
        TermsOfService = new Uri("https://example/terms"),
        Contact = new OpenApiContact
        {
            Name = "Sindre Steinsvik & Sondre K. Fjelde",
            Email = "example@email.com",
            Url = new Uri("https://gitlab.com/SondreFjelde/movieapi/-/tree/main/MovieDbApi"),
        },
        License = new OpenApiLicense
        {
            Name = "Use under MIT",
            Url = new Uri("https://opensource.org/licenses/MIT"),
        }
    });
    var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
    var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
    c.IncludeXmlComments(xmlPath);
});

builder.Services.AddDbContext<MovieDbContext>(
    opt => opt.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection")));

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
