﻿using MovieDbApi.Models;

namespace MovieDbApi.Services
{
    public interface ICharacterService
    {
        /// <summary>
        /// Adds a character
        /// </summary>
        /// <param name="character"></param>
        /// <returns></returns>
        public Task<Character> AddCharacterAsync(Character character);
        /// <summary>
        /// Checks if a character exists
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool CharacterExists(int id);
        /// <summary>
        /// Gets all characters
        /// </summary>
        /// <returns></returns>
        public Task<IEnumerable<Character>> GetAllCharactersAsync();
        /// <summary>
        /// Gets a character from ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task<Character> GetCharacterByIdAsync(int id);
        /// <summary>
        /// Deletes a character from ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task DeleteCharacterAsync(int id);
        /// <summary>
        /// Updates a character
        /// </summary>
        /// <param name="character"></param>
        /// <returns></returns>
        public Task PutCharacterAsync(Character character);
        /// <summary>
        /// Updates a character
        /// </summary>
        /// <param name="character"></param>
        /// <returns></returns>
        public Task UpdateCharacterAsync(Character character);
    }
}
