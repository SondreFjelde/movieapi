﻿using MovieDbApi.Models;

namespace MovieDbApi.Services
{
    public interface IFranchiseService
    {
        /// <summary>
        /// Creates a franchise and adds it to DB
        /// </summary>
        /// <param name="franchise"></param>
        /// <returns></returns>
        public Task<Franchise> AddFranchiseAsync(Franchise franchise);
        /// <summary>
        /// Checks if a franchise exists
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool FranchiseExists(int id);
        /// <summary>
        /// Gets all franchises in DB
        /// </summary>
        /// <returns></returns>
        public Task<IEnumerable<Franchise>> GetAllFranchisesAsync();
        /// <summary>
        /// Gets a franchise by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task<Franchise> GetFranchiseByIdAsync(int id);
        /// <summary>
        /// Gets all movies in a franchise
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task<IEnumerable<Movie>> GetAllMoviesInFranchiseAsync(int id);
        /// <summary>
        /// Gets all characters who played in movies in a franchise
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task<IEnumerable<Character>> GetAllCharactersInFranchise(int id);
        /// <summary>
        /// Updates a franchise
        /// </summary>
        /// <param name="franchise"></param>
        /// <returns></returns>
        public Task PutFranchiseAsync(Franchise franchise);
        /// <summary>
        /// Updates a franchise
        /// </summary>
        /// <param name="franchise"></param>
        /// <returns></returns>
        public Task UpdateFranchiseAsync(Franchise franchise);
        /// <summary>
        /// Updates movies in a franchise
        /// </summary>
        /// <param name="franchiseId"></param>
        /// <param name="movie"></param>
        /// <returns></returns>
        public Task UpdateMoviesInFranchiseAsync(int franchiseId, List<int> movie);
        /// <summary>
        /// Deletes a franchise
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task DeleteFranchiseAsync(int id);
    }
}