﻿using MovieDbApi.Services;
using MovieDbApi.Models;
using Microsoft.EntityFrameworkCore;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading.Tasks;
using MovieDbApi.DTO.CharacterDTO;

namespace MovieDbApi.Services
{
    public class MovieService : IMovieService
    {
        private readonly MovieDbContext _context;

        public MovieService(MovieDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Adds movie object as an entry to Movies database and returns the object that was used to add
        /// </summary>
        /// <param name="character"></param>
        /// <returns></returns>
        public async Task<Movie> AddMovieAsync(Movie movie)
        {
            _context.Movies.Add(movie);
            await _context.SaveChangesAsync();
            return movie;
        }

        /// <summary>
        /// Returns truth statement whether a movie of specified id exists in database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool MovieExists(int id)
        {
            return _context.Movies.Any(e => e.Id == id);
        }

        /// <summary>
        /// Asynchronously gets all movies from Movies db table, 
        /// finds all characters who played in each movie and returns it as a list
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<Movie>> GetAllMoviesAsync()
        {
            return await _context.Movies
                .Include(m => m.Characters)
                .ToListAsync();
        }

        /// <summary>
        /// Finds movie with specified id and returns it if it exists
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Movie> GetMovieByIdAsync(int id)
        {
            return await _context.Movies.FindAsync(id);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<IEnumerable<Character>> GetAllCharactersInMovieAsync(int id)
        {

            return await _context.Characters
                .Include(c => c.Movies)
                .Where(c => c.Movies.Any(m => m.Id == id))
                .ToListAsync();

        }
        /// <summary>
        /// Updates specified movie entry in database
        /// </summary>
        /// <param name="character"></param>
        /// <returns></returns>
        public async Task UpdateMovieAsync(Movie movie)
        {
            _context.Entry(movie).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task PutMovieAsync(Movie movie)
        {
            _context.Entry(movie).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }


        public async Task UpdateCharactersInMovieAsync(int movieId, List<int> characters)
        {
            Movie movieToUpdate = await _context.Movies
                .Include(m => m.Characters)
                .Where(m => m.Id == movieId)
                .FirstAsync();

            List<Character> charList = new();
            foreach (int charId in characters)
            {
                Character? c = await _context.Characters.FindAsync(charId);
                if (c == null)
                {
                    throw new KeyNotFoundException();
                }
                charList.Add(c);
            }
            movieToUpdate.Characters = charList;
            await _context.SaveChangesAsync();
        }

        public async Task DeleteMovieAsync(int id)
        {
            var movieToDel = await _context.Movies.FindAsync(id);
            _context.Movies.Remove(movieToDel);
            await _context.SaveChangesAsync();
        }
    }
}