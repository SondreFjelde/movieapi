﻿
using Microsoft.EntityFrameworkCore;
using MovieDbApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;



namespace MovieDbApi.Services
{
    public class CharacterService : ICharacterService
    {
        private readonly MovieDbContext _context;
        public CharacterService(MovieDbContext context)
        {
            _context = context;
        }
        /// <summary>
        /// Adds character object as an entry to Characters database and returns the object that was used to add
        /// </summary>
        /// <param name="character"></param>
        /// <returns></returns>
        public async Task<Character> AddCharacterAsync(Character character)
        {
            _context.Characters.Add(character);
            await _context.SaveChangesAsync();
            return character;
        }
        /// <summary>
        /// Returns truth statement whether a character of specified id exists in database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool CharacterExists(int id)
        {
            return _context.Characters.Any(c => c.Id == id);
        }
        /// <summary>
        /// Asynchronously deletes character with the specified id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task DeleteCharacterAsync(int id)
        {
            var charToDel = await _context.Characters.FindAsync(id);
            _context.Characters.Remove(charToDel);
            await _context.SaveChangesAsync();
        }
        /// <summary>
        /// Asynchronously gets all characters from Characters db table, 
        /// finds all movies each character is a part of and returns it as list
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<Character>> GetAllCharactersAsync()
        {
            return await _context.Characters
                .Include(c => c.Movies)
                .ToListAsync();
        }
        /// <summary>
        /// Finds character with specified id and returns it if it exists
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Character> GetCharacterByIdAsync(int id)
        {
            return await _context.Characters.FindAsync(id);
        }

        public async Task PutCharacterAsync(Character character)
        {
            _context.Entry(character).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Updates specified character entry in database
        /// </summary>
        /// <param name="character"></param>
        /// <returns></returns>
        public async Task UpdateCharacterAsync(Character character)
        {
            _context.Entry(character).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
    }
}
