﻿using MovieDbApi.Models;

namespace MovieDbApi.Services
{
    public interface IMovieService
    {
        /// <summary>
        /// Checks if a movie exists
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool MovieExists(int id);
        /// <summary>
        /// Adds a movie to DB
        /// </summary>
        /// <param name="movie"></param>
        /// <returns></returns>
        public Task<Movie> AddMovieAsync(Movie movie);
        /// <summary>
        /// Gets all movies in DB
        /// </summary>
        /// <returns></returns>
        public Task<IEnumerable<Movie>> GetAllMoviesAsync();
        /// <summary>
        /// Gets a movie by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task<Movie> GetMovieByIdAsync(int id);
        /// <summary>
        /// Gets all characters who played in a movie
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task<IEnumerable<Character>> GetAllCharactersInMovieAsync(int id);
        /// <summary>
        /// Updates characters in a movie
        /// </summary>
        /// <param name="id"></param>
        /// <param name="characters"></param>
        /// <returns></returns>
        public Task UpdateCharactersInMovieAsync(int id, List<int> characters);
        /// <summary>
        /// Updates a movie
        /// </summary>
        /// <param name="movie"></param>
        /// <returns></returns>
        public Task UpdateMovieAsync(Movie movie);
        /// <summary>
        /// Updates a movie
        /// </summary>
        /// <param name="movie"></param>
        /// <returns></returns>
        public Task PutMovieAsync(Movie movie);
        /// <summary>
        /// deletes a movie
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task DeleteMovieAsync(int id);
    }
}
