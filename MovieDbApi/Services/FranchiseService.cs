﻿using MovieDbApi.Services;
using MovieDbApi.Models;
using Microsoft.EntityFrameworkCore;

namespace MovieDbApi.Services
{
    public class FranchiseService : IFranchiseService
    {
        private readonly MovieDbContext _context;

        public FranchiseService(MovieDbContext context)
        {
            _context = context;
        }

        public async Task<Franchise> AddFranchiseAsync(Franchise franchise)
        {
            _context.Franchises.Add(franchise);
            await _context.SaveChangesAsync();
            return franchise;
        }

        public async Task DeleteFranchiseAsync(int id)
        {
            var franchiseToDel = await _context.Franchises.FindAsync(id);
            _context.Franchises.Remove(franchiseToDel);
            await _context.SaveChangesAsync();
        }

        public bool FranchiseExists(int id)
        {
            return _context.Franchises.Any(franchise => franchise.Id == id);
        }

        public async Task<IEnumerable<Character>> GetAllCharactersInFranchise(int id)
        {
            return await _context.Characters
                .Include(c => c.Movies)
                .Where(c => c.Movies.Any(m => m.FranchiseId == id))
                .ToListAsync();
        }

        public async Task<IEnumerable<Franchise>> GetAllFranchisesAsync()
        {
            return await _context.Franchises
                .Include(f => f.Movies)
                .ToListAsync();
        }

        public async Task<IEnumerable<Movie>> GetAllMoviesInFranchiseAsync(int id)
        {
            return await _context.Movies
                .Include(f => f.Franchise)
                .Where(f => f.FranchiseId == id)
                .ToListAsync();
        }

        public async Task<Franchise> GetFranchiseByIdAsync(int id)
        {
            return await _context.Franchises.FindAsync(id);
        }

        public async Task PutFranchiseAsync(Franchise franchise)
        {
            _context.Entry(franchise).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task UpdateFranchiseAsync(Franchise franchise)
        {
            _context.Entry(franchise).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task UpdateMoviesInFranchiseAsync(int franchiseId, List<int> movie)
        {
            Franchise franchiseToUpdate = await _context.Franchises
                .Include(f => f.Movies)
                .Where(f => f.Id == franchiseId)
                .FirstAsync();

            List<Movie> movieList = new();
            foreach (int movieId in movie)
            {
                Movie? m = await _context.Movies.FindAsync(movieId);
                if (m == null)
                {
                    throw new KeyNotFoundException();
                }
                movieList.Add(m);
            }
            franchiseToUpdate.Movies = movieList;
            await _context.SaveChangesAsync();
        }
    }
}
