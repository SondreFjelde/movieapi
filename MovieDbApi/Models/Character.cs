﻿using MovieDbApi.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieDbApi.Models
{
    public class Character
    {
        public int Id { get; set; }
        [MaxLength(100)] public string? Name { get; set; }
        [MaxLength(100)] public string? Alias { get; set; }
        [MaxLength(100)] public string? Gender { get; set; }
        [MaxLength(300)] public string? PictureLink { get; set; }
        public virtual ICollection<Movie>? Movies { get; set; }
        
    }
}
