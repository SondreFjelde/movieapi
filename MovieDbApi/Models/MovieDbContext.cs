﻿using Microsoft.EntityFrameworkCore;
using MovieDbApi.Models;
using System.Diagnostics.CodeAnalysis;

namespace MovieDbApi.Models
{
    public class MovieDbContext : DbContext
    {
        public virtual DbSet<Movie>? Movies { get; set; }
        public virtual DbSet<Character>? Characters { get; set; }
        public virtual DbSet<Franchise>? Franchises { get; set; }
        


        public MovieDbContext([NotNullAttribute] DbContextOptions options) : base(options)
        {
        
        }



        


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //Creates the relationship between movies and franchise
            modelBuilder.Entity<Franchise>()
               .HasMany(e => e.Movies)
               .WithOne(e => e.Franchise)
               .OnDelete(DeleteBehavior.SetNull);


            modelBuilder.Entity<Character>().HasData(new Character
            {
                Id = 1,
                Name = "Jack Torrance",
                Alias = "Jack Nicholson",
                Gender = "male",
                PictureLink = "https://m.media-amazon.com/images/M/MV5BMTQ3OTY0ODk0M15BMl5BanBnXkFtZTYwNzE4Njc4._V1_UY317_CR7,0,214,317_AL_.jpg",
            });
            modelBuilder.Entity<Character>().HasData(new Character
            {
                Id = 2,
                Name = "Wendy Torrance",
                Alias = "Shelley Duvall",
                Gender = "female",
                PictureLink = "https://m.media-amazon.com/images/M/MV5BNjE4MTY1Mzk0N15BMl5BanBnXkFtZTgwOTQzNDE2MDE@._V1_UX214_CR0,0,214,317_AL_.jpg",
            });

            modelBuilder.Entity<Character>().HasData(new Character
            {
                Id = 3,
                Name = "Dan Torrance",
                Alias = "Ewan McGregor",
                Gender = "male",
                PictureLink = "https://m.media-amazon.com/images/M/MV5BNjE4MTY1Mzk0N15BMl5BanBnXkFtZTgwOTQzNDE2MDE@._V1_UX214_CR0,0,214,317_AL_.jpg",
            });

            modelBuilder.Entity<Character>().HasData(new Character
            {
                Id = 4,
                Name = "Strange",
                Alias = "Benedict Cumberbatch",
                Gender = "female",
                PictureLink = "https://m.media-amazon.com/images/M/MV5BNjE4MTY1Mzk0N15BMl5BanBnXkFtZTgwOTQzNDE2MDE@._V1_UX214_CR0,0,214,317_AL_.jpg",
            });


            // Franchise entities
            modelBuilder.Entity<Franchise>().HasData(new Franchise
            {
                Id = 1,
                Name = "The Shining",
                Description = "Haunted by a persistent writer's block, the aspiring author and recovering alcoholic, " +
                "Jack Torrance, drags his wife, Wendy, and his gifted son, Danny, up snow-capped Colorado's secluded Overlook Hotel " +
                "after taking up a job as an off-season caretaker. As the cavernous hotel shuts down for the season, the manager gives Jack a " +
                "grand tour, and the facility's chef, the ageing Mr Hallorann, has a fascinating chat with Danny about a rare psychic gift " +
                "called \"The Shining\", making sure to warn him about the hotel's abandoned rooms, and, in particular, the off-limits Room 237. " +
                "However, instead of overcoming the dismal creative rut, little by little, Jack starts losing his mind, trapped in an unforgiving " +
                "environment of seemingly endless snowstorms, and a gargantuan silent prison riddled with strange occurrences and eerie visions. " +
                "Now, the incessant voices inside Jack's head demand sacrifice. Is Jack capable of murder? —Nick Riganas",


            });

            modelBuilder.Entity<Franchise>().HasData(new Franchise
            {
                Id = 2,
                Name = "Marvel Cinematic Universe",
                Description = "Cartoons, but live action",


            });


            modelBuilder.Entity<Movie>().HasData(new Movie
            {
                Id = 1,
                Title = "The Shining",
                Genre = "Horror",
                ReleaseYear = 1980,
                Director = "Stanley Kubrick",
                Picture = "https://www.imdb.com/title/tt0081505/mediaviewer/rm3901111552/?ref_=tt_ov_i",
                TrailerLink = "https://www.imdb.com/video/vi2689121305?playlistId=tt0081505&ref_=tt_ov_vi",
                FranchiseId = 1,
            });

            modelBuilder.Entity<Movie>().HasData(new Movie
            {
                Id = 2,
                Title = "Doctor Sleep",
                Genre = "Horror",
                ReleaseYear = 2019,
                Director = "Mike Flanagan",
                Picture = "https://www.imdb.com/title/tt5606664/mediaviewer/rm850561793/?ref_=tt_ov_i",
                TrailerLink = "https://www.imdb.com/video/vi212254489?playlistId=tt5606664&ref_=tt_ov_vi",
                FranchiseId = 1,
            });

            modelBuilder.Entity<Movie>().HasData(new Movie
            {
                Id = 3,
                Title = "Doctor Strange",
                Genre = "Horror",
                ReleaseYear = 2017,
                Director = "Mike Flanagan",
                Picture = "https://www.imdb.com/title/tt5606664/mediaviewer/rm850561793/?ref_=tt_ov_i",
                TrailerLink = "https://www.imdb.com/video/vi212254489?playlistId=tt5606664&ref_=tt_ov_vi",
                FranchiseId = 2,
            });


           
            // Creates the relationship between characters and movies
            modelBuilder.Entity<Movie>()
                .HasMany(p => p.Characters)
                .WithMany(m => m.Movies)
                .UsingEntity<Dictionary<string, object>>(
                    "MovieCharacters",
                    r => r.HasOne<Character>().WithMany().HasForeignKey("CharacterId"),
                    l => l.HasOne<Movie>().WithMany().HasForeignKey("MovieId"),
                    je =>
                    {
                        je.HasKey("MovieId", "CharacterId");
                        je.HasData(
                            new { MovieId = 1, CharacterId = 1 },
                            new { MovieId = 2, CharacterId = 3 },
                            new { MovieId = 3, CharacterId = 4 });
                     });


         

        }
    }
}
