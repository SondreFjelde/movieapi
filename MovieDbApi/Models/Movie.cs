﻿using MovieDbApi.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieDbApi.Models
{
    public class Movie
    {
        public int Id { get; set; }
        [MaxLength(100)] public string? Title { get; set; }
        [MaxLength(100)] public string? Genre { get; set; }
        public int ReleaseYear { get; set; }
        [MaxLength(100)] public string? Director { get; set; }
        [MaxLength(300)] public string? Picture { get; set; }
        [MaxLength(300)] public string? TrailerLink { get; set; }

        public virtual ICollection<Character>? Characters { get; set; }

        public int? FranchiseId { get; set; }   
        public Franchise? Franchise { get; set; }
    }
}
