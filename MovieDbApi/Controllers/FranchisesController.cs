﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieDbApi.DTO.CharacterDTO;
using MovieDbApi.DTO.FranchiseDTO;
using MovieDbApi.DTO.MovieDTO;
using MovieDbApi.Models;
using MovieDbApi.Services;
using NuGet.DependencyResolver;

namespace MovieDbApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class FranchisesController : ControllerBase
    {
        private readonly MovieDbContext _context;
        private readonly IMapper _mapper;
        private readonly IFranchiseService _franchiseService;

        public FranchisesController(MovieDbContext context, IMapper mapper, IFranchiseService franchiseService)
        {
            _context = context;
            _mapper = mapper;
            _franchiseService = franchiseService;        
        }

        // POST: api/Franchises
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /// <summary>
        /// Creates a franchise and adds it to DB
        /// </summary>
        /// <param name="dtofranchise"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<Franchise>> PostFranchise(FranchiseCreateDTO dtofranchise)
        {
            if (_context.Franchises == null)
            {
                return Problem("Entity set 'MovieDbContext.Franchises'  is null.");
            }
            Franchise domainFranchise = _mapper.Map<Franchise>(dtofranchise);
            domainFranchise = await _franchiseService.AddFranchiseAsync(domainFranchise);

            return CreatedAtAction("GetFranchise", new { id = domainFranchise.Id }, _mapper.Map<FranchiseReadDTO>(domainFranchise));
        }

        // GET: api/Franchisess
        /// <summary>
        /// Gets all franchises in DB
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchiseReadDTO>>> GetFranchises()
        {
            return _mapper.Map<List<FranchiseReadDTO>>(await _franchiseService.GetAllFranchisesAsync());
        }


        // GET: api/Franchises/5
        /// <summary>
        /// Gets a franchise in DB from ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseReadDTO>> GetFranchise(int id)
        {
            if (!_franchiseService.FranchiseExists(id))
          {
              return NotFound();
          }
            var franchise = await _franchiseService.GetFranchiseByIdAsync(id);

            if (franchise == null)
            {
                return NotFound();
            }

            return _mapper.Map<FranchiseReadDTO>(await _franchiseService.GetFranchiseByIdAsync(id));
        }

        /// <summary>
        /// Gets all characters who played in movies from a specific franchise
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}/Characters")]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetAllCharactersInFranchise(int id)
        {
            if (!_franchiseService.FranchiseExists(id))
            {
                return NotFound();
            }
            return _mapper.Map<List<CharacterReadDTO>>(await _franchiseService.GetAllCharactersInFranchise(id));
        }

        /// <summary>
        /// Gets all movies in a Franchise
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}/Movies")]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetMoviesInFranchise(int id)
        {
            return _mapper.Map<List<MovieReadDTO>>(await _franchiseService.GetAllMoviesInFranchiseAsync(id));
        }

        // PUT: api/Franchises/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /// <summary>
        /// Updates the data of a specific franchise from ID
        /// </summary>
        /// <param name="id"></param>
        /// <param name="dtofranchise"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFranchise(int id, FranchiseEditDTO dtofranchise)
        {
            if (id != dtofranchise.Id)
            {
                return BadRequest();
            }

            Franchise domainFranchise = _mapper.Map<Franchise>(dtofranchise);
            await _franchiseService.PutFranchiseAsync(domainFranchise);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FranchiseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Updates the movies of a database
        /// </summary>
        /// <param name="id"></param>
        /// <param name="movies"></param>
        /// <returns></returns>
        [HttpPut("{id}/Movies")]
        public async Task<IActionResult> UpdateMoviesInFranchise(int id, List<int> movies)
        {
            if (!_franchiseService.FranchiseExists(id))
            {
                return NotFound();
            }
            try
            {
                await _franchiseService.UpdateMoviesInFranchiseAsync(id, movies);
            }
            catch (KeyNotFoundException)
            {
                return BadRequest("Invalid Movie");
            }
            return NoContent();
        }

        // DELETE: api/Franchises/5
        /// <summary>
        /// Deletes a franchise by ID from DB
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFranchise(int id)
        {
            if (_context.Franchises == null)
            {
                return NotFound();
            }
            var franchise = await _context.Franchises.FindAsync(id);
            if (franchise == null)
            {
                return NotFound();
            }

            await _franchiseService.DeleteFranchiseAsync(id);

            return NoContent();
        }

        private bool FranchiseExists(int id)
        {
            return (_context.Franchises?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
