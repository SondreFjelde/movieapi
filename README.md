Assignment 3: MovieDbApi

Description
This project is the assignment 3 in the Noroff .NET Accelerate program. 
The project consists of setting up the relationship between Movies, Characters and Franchises. Setting up data requirements for the different classes. And seeding some dummy data for Appendix A.
In Appendix B we created the different endpoints for the different Controllers, created different DTOs and used Automapper, used Swagger for documentation and crated services for the diffrent endpoints

Contributors
The contributors of this project is: Sondre Fjelde and Sindre Tornes Steinsvik
